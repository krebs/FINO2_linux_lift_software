"""
-Battery Voltage
-change just date to date+time
"""

import os
import re
import psutil
import datetime
import time

with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), r'../output/dht_log')) as f:
    dht_log_raw = f.readlines()

dht_last_hour = dht_log_raw[-7:-1]


hdd = psutil.disk_usage('/')
#temps = psutil.sensors_temperatures()
last_boot = psutil.boot_time()
cpu = psutil.cpu_percent(interval=1)
mem_virt = psutil.virtual_memory()

print("SD Storage: " + str(hdd.used/1e9) + " / " + str(hdd.total/1e9) + " GB used")
print("RAM: " + str(mem_virt.used/1e9) + " / " + str(mem_virt.total/1e9) + " GB used")
print("Current CPU usage: " + str(cpu) + " %")
print("Last reboot at: " + str(datetime.datetime.fromtimestamp(last_boot)))


file_name = time.strftime("%Y-%m-%d_monitoring_telegram.txt", time.localtime())
with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), r'../output/monitoring_telegram_archive/', file_name),'w') as file:
    file.write(time.strftime("monitoring telegram for %Y-%m-%d\n", time.localtime()))
    file.writelines(dht_last_hour)
    file.writelines("SD Storage: " + str(hdd.used / 1e9) + " / " + str(hdd.total / 1e9) + " GB used\n")
    file.writelines("RAM: " + str(mem_virt.used / 1e9) + " / " + str(mem_virt.total / 1e9) + " GB used\n")
    file.writelines("Current CPU usage: " + str(cpu) + " %\n")
    file.writelines("Last reboot at: " + str(datetime.datetime.fromtimestamp(last_boot))+"\n")



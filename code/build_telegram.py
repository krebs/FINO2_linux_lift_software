"""
-change just date to date+time
"""
import os
import re
import time

with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), r'../output/FLNTUS_test_data2.csv')) as f:
    flntus_scan_raw = f.readlines()
with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), r'../output/SBE19p_test_data2.csv')) as f:
    sbe19_scan_raw = f.readlines()


#read in SBE19
index_limits = []
for ind, line in enumerate(sbe19_scan_raw):
    if re.search('SBE19p*', line):
        #print(line.strip())
        index_limits.append(ind)
        if len(index_limits) == 2:
            break
if len(index_limits) == 2:
    sbe19_scan_data = sbe19_scan_raw[index_limits[0]:index_limits[1]]
elif len(index_limits) == 0:
    print("PUT AN ERROR INTO THE TELEGRAM HERE")
    sbe19_scan_data = []
else:
    print("PUT AN ERROR INTO THE TELEGRAM HERE")
    sbe19_scan_data = []

#read in FLNTUS
index_limits = []
for ind, line in enumerate(flntus_scan_raw):
    if re.search('FLNTUS profile*', line) or re.search('End of profile*', line):
        #print(line.strip())
        index_limits.append(ind)
        if len(index_limits) == 2:
            break
if len(index_limits) == 2:
    flntus_scan_data = flntus_scan_raw[index_limits[0]:index_limits[1]]
elif len(index_limits) == 0:
    print("PUT AN ERROR INTO THE TELEGRAM HERE")
    flntus_scan_data = []
else:
    print("PUT AN ERROR INTO THE TELEGRAM HERE")
    flntus_scan_data = []


file_name = time.strftime("%Y-%m-%d_data_telegram.txt", time.localtime())
with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), r'../output/data_telegram_archive/', file_name),'w') as file:
    file.writelines(sbe19_scan_data)
    file.writelines("\n")
    file.writelines(flntus_scan_data)


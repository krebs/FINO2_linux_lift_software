import time
import board
import adafruit_dht

#print("Running dht measurement...")
file_name=time.strftime("%Y-%m-%d_dht_data.txt", time.localtime())

# you can pass DHT22 use_pulseio=False if you wouldn't like to use pulseio.
# This may be necessary on a Linux single board computer like the Raspberry Pi,
# but it will not work in CircuitPython.
dhtDevice = adafruit_dht.DHT22(board.D27, use_pulseio=False)
time.sleep(2)

measure_time=time.strftime("%Y-%m-%dT%H:%M:%S%z",time.localtime())
try:
    # Print the values to the serial port
    temperature_c = dhtDevice.temperature
    humidity = dhtDevice.humidity/100
    #print(measure_time+
     #   ", Temp:\t{:.1f} C,\tHumidity: {}".format(
      #      temperature_c, humidity
       # ))
    with open('/home/pi/Desktop/FINO2_linux_lift_software/output/dht_archive/'+file_name,'a') as data_file:
        data_file.write(measure_time+", Temp:\t{:.1f} C,\tHumidity: {:.3f}\n".format(temperature_c, humidity))
    print(measure_time+", Temp:\t{:.1f} C,\tHumidity: {:.3f}".format(temperature_c, humidity))

except RuntimeError as error:
    # Errors happen fairly often, DHT's are hard to read, just keep going
    print(error.args[0])
    with open('/home/pi/Desktop/FINO2_linux_lift_software/output/dht_archive/'+file_name,'a') as data_file:
        data_file.write(measure_time+", "+error.args[0]+'\n')
        
except Exception as error:
    dhtDevice.exit()
    raise error
    with open('/home/pi/Desktop/FINO2_linux_lift_software/output/dht_archive/'+file_name,'a') as data_file:
        data_file.write(measure_time+", "+error.args[0]+'\n')
    print(error.args[0])
dhtDevice.exit()



    
    

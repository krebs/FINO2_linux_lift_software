# -*- coding: utf-8 -*-
"""
Created on Mon Jul  4 12:01:38 2022
@author: krebs
 _____________________TODO__________________________
-(currently running jobs?)
_________________________TODO_________________________
"""


import PySimpleGUI as sg
import json
import os
import numpy as np
from datetime import datetime
import subprocess
from sys import platform as sysplat
from crontab import CronTab
import multiprocessing

from modules.SBE19p_profiling_script import run_sbe19plus_scan
from modules.FLNTUS_profiling_script import run_flntus_scan

file_path=os.path.dirname(os.path.abspath(__file__))


# ----CRONTAB STUFF--
if sysplat == 'linux':
    cron = CronTab(user=True)
elif sysplat == 'win32':
    cron = CronTab(tabfile=r'D:\Python\fino2_software_gui\data\test_tab.tab')
else:
    cron = 0
    print("ERROR: UNKNOWN OPERATING SYSTEM")
# ----CRONTAB STUFF


class LogfilesCheckForUpdates(object):
    def __init__(self, filepath):
        self._cached_stamp = 0
        self.filename = filepath

    def check_for_update(self):
        if os.path.isfile(self.filename):
            stamp = os.stat(self.filename).st_mtime
            if stamp != self._cached_stamp:
                self._cached_stamp = stamp
                return True
            else:
                return False


def check_for_logfile_changes():
    if term0_modstatus.check_for_update():
        with open(term0_modstatus.filename) as file:
            window_main["-TERMINAL_OUTPUT_0-"].update(file.read())
    if term1_modstatus.check_for_update():
        with open(term1_modstatus.filename) as file:
            window_main["-TERMINAL_OUTPUT_1-"].update(file.read())
    if term2_modstatus.check_for_update():
        with open(term2_modstatus.filename) as file:
            window_main["-TERMINAL_OUTPUT_2-"].update(file.read())
    if term3_modstatus.check_for_update():
        with open(term3_modstatus.filename) as file:
            window_main["-TERMINAL_OUTPUT_3-"].update(file.read())
    if cronlog_modstatus.check_for_update():
        with open('/var/log/cron.log') as file:
            window_main["-CRONLOG-"].update(file.read())


def print_active_sensors():
    with open(os.path.join(file_path,'../data/sensor_params.json')) as json_file:
        sensor_params = json.load(json_file)
    output = []
    for i in range(0, 4):
        if sensor_params["-SENSOR" + str(i) + "ENABLED-"]:
            output.append("Name:\t" + sensor_params["-SENSOR" + str(i) + "NAME-"] + "\n")
            output.append("SN:\t" + sensor_params["-SENSOR" + str(i) + "SN-"] + "\n")
            output.append("Port:\t" + "ttyUSB" + str(i) + "\n")
            output.append("Baud:\t" + sensor_params["-SENSOR" + str(i) + "BAUD-"] + "\n")
            output.append("Script:\t" + sensor_params["-SENSOR" + str(i) + "SCRIPT-"] + "\n")
            output.append("\n")
    output = "".join(output)
    return output


# ----------------------------Logfile stuff
with open(os.path.join(file_path,'../data/logfilepaths.txt'), 'r') as file:
    data = file.readlines()
    term0_modstatus = LogfilesCheckForUpdates(filepath=data[0].strip())
    term1_modstatus = LogfilesCheckForUpdates(filepath=data[1].strip())
    term2_modstatus = LogfilesCheckForUpdates(filepath=data[2].strip())
    term3_modstatus = LogfilesCheckForUpdates(filepath=data[3].strip())
cronlog_modstatus = LogfilesCheckForUpdates('/var/log/cron.log')

# ----------------------------Cron stuff

def get_scheduled_job_list(cron):
    job_line = []
    for i, job in enumerate(cron):
        schedule = job.schedule()
        if job.render()[0:7]!= '@reboot' and job.render()[0] != '#' :
            job_line.append(
                str(schedule.get_next()) + " | " + "Job " + str(i) + " | " + str(job.comment))
            job_line.append(
                str(schedule.get_next()) + " | " + "Job " + str(i) + " | " + str(job.comment))
    job_line.sort()
    job_line = "\n".join(job_line)
    return job_line
    


# Constuct main window layout
def main_window(job_line):
    with open(os.path.join(file_path,'../data/scan_params.json')) as json_file:
        scan_params = json.load(json_file)
    with open(os.path.join(file_path,'../data/logfilepaths.txt'), 'r') as file:
        logfilepaths = file.readlines()

    # Main Window
    cronjobs_frame = sg.Frame("Next jobs", [
        [sg.Multiline(get_scheduled_job_list(cron), size=(70, 10), key="-CRONJOBS-")]], key="-JOB_FRAME_TITLE_CLOCK-")

    history_frame = sg.Frame("Completed jobs", [
        [sg.Multiline("PLACEHOLDER FOR HISTORY OF COMPLETED OR FAILED JOBS", size=(70, 10), key="-CRONLOG-", autoscroll=True)]
    ])
    job_column = sg.Column([[cronjobs_frame], [history_frame]])

    # FRAME FOR FAILED JOBS
    misc_script_frame = sg.Frame("Active misc. scripts", [
        [sg.Button("Add job"), sg.Button('Remove job')],
        [sg.Multiline(print_active_sensors(), size=(30, 7), key="-ACTIVE_SCRIPT_OVERVIEW-")]
    ])

    sensor_frame = sg.Frame("Active sensor scripts", [
        [sg.Multiline(print_active_sensors(), size=(30, 20), key="-ACTIVE_SENSOR_OVERVIEW-")],
        [sg.Button("ttyUSB Devices")],
        [sg.Button("Test Sensor Connectivity")]
    ])
    script_frame = sg.Column([[misc_script_frame], [sensor_frame]])
    scan_settings_frame = sg.Frame("Scan settings", [
        [sg.Text("Scan duration: " + str(scan_params["-SCAN_DURATION-"]) + "s", key="-SHOW_SCAN_DUR-")],
        [sg.Text("Scan interval: " + str(scan_params["-SCAN_INTERVAL-"]) + "s", key="-SHOW_SCAN_INTERVAL-")],
        [sg.Text("Interval start at: " + str(scan_params["-START_DATE-"]), key="-SHOW_SCAN_DATE-")],
        [sg.Text("Scan resolution (if applicable): " + str(scan_params["-SCAN_RESOLUTION-"]) + "Hz",
                 key="-SHOW_SCAN_RESOLUTION-")],
        [sg.Text("Pre-pump / warmup duration: " + str(scan_params["-SCAN_WARMUP_TIME-"]) + "s",
                 key="-SHOW_SCAN_WARMUP_TIME-")],
        [sg.Button("Run Job now"), sg.Button("Start at interval")],
        [sg.Button("STOP SCAN"), sg.Button("Change scan settings")]
    ])
    terminal_column = [
        [sg.Input(logfilepaths[0].strip(), key="-LOG_0_INPUT_PATH-", enable_events=False),
         sg.FileBrowse(target="-LOG_0_INPUT_PATH-")],
        [sg.Multiline("No readout", size=(50, 13), key="-TERMINAL_OUTPUT_0-", autoscroll=True)],

        [sg.Input(logfilepaths[1].strip(), key="-LOG_1_INPUT_PATH-", enable_events=False),
         sg.FileBrowse(target="-LOG_1_INPUT_PATH-")],
        [sg.Multiline("No readout", size=(50, 13), key="-TERMINAL_OUTPUT_1-", autoscroll=True)],

        [sg.Input(logfilepaths[2].strip(), key="-LOG_2_INPUT_PATH-", enable_events=False),
         sg.FileBrowse(target="-LOG_2_INPUT_PATH-")],
        [sg.Multiline("No readout", size=(50, 13), key="-TERMINAL_OUTPUT_2-", autoscroll=True)],

        [sg.Input(logfilepaths[3].strip(), key="-LOG_3_INPUT_PATH-", enable_events=False),
         sg.FileBrowse(target="-LOG_3_INPUT_PATH-")],
        [sg.Multiline("No readout", size=(50, 13), key="-TERMINAL_OUTPUT_3-", autoscroll=True)],
        [sg.Button("Reload Logpaths")]

        #[sg.Text("Terminal input: "), sg.Input("", size=(20, 1), key="-TERM_INPUT-"),
        # sg.Button("Run", bind_return_key=False)],
        #[sg.Multiline(size=(50, 7), key="-TERMINAL_OUTPUT_test-", autoscroll=True)]
    ]

    layout_main = [
        [job_column, sg.VSeparator(), script_frame,
         sg.VSeperator(), scan_settings_frame, sg.VSeperator(), sg.Column(terminal_column)]
    ]
    return layout_main


# Change settings for the scan
def change_scan_settings_window():
    with open(os.path.join(file_path,'../data/scan_params.json')) as json_file:
        scan_params = json.load(json_file)

    layout_settings = [
        [sg.Text("Scan duration [s]: "),
         sg.Spin(initial_value=scan_params["-SCAN_DURATION-"], values=list(np.linspace(1, 9999, 9999)),
                 size=(8, 1), key="-SCAN_DURATION-")],
        [sg.Text("Scan interval [s]: "),
         sg.Spin(initial_value=scan_params["-SCAN_INTERVAL-"], values=list(np.linspace(1, 9999, 9999)),
                 size=(8, 1), key="-SCAN_INTERVAL-")],
        [sg.Text("Interval start: "), sg.Input(default_text=scan_params["-START_DATE-"], key="-START_DATE-"),
         sg.CalendarButton("Choose Date")],
        [sg.Text("Scan resolution [Hz] (if applicable, 0 for max): "),
         sg.Spin(initial_value=scan_params["-SCAN_RESOLUTION-"],
                 values=list(np.linspace(0, 10, 21)), size=(6, 1), key="-SCAN_RESOLUTION-")],
        [sg.Text("Pre-pump / warmup duration [s]"),
         sg.Spin(initial_value=scan_params["-SCAN_WARMUP_TIME-"],
                 values=list(np.linspace(1, 999, 999)), size=(6, 1), key="-SCAN_WARMUP_TIME-")],
        [sg.Button("Save"), sg.Button("Cancel")]
    ]
    window_scan_settings = sg.Window("Change scan settings", layout_settings)
    while True:
        event, values = window_scan_settings.read()
        if event == sg.WIN_CLOSED or event == 'Cancel':
            break
        elif event == "Save":
            json_file = json.dumps(values)
            # open file for writing, "w" 
            f = open("../data/scan_params.json", "w")
            f.write(json_file)
            f.close()
            window_main["-SHOW_SCAN_DUR-"].update("Scan duration: " + str(values["-SCAN_DURATION-"]) + "s")
            window_main["-SHOW_SCAN_INTERVAL-"].update("Scan interval: " + str(values["-SCAN_INTERVAL-"]) + "s")
            window_main["-SHOW_SCAN_DATE-"].update("Interval start at: " + str(values["-START_DATE-"]))
            window_main["-SHOW_SCAN_RESOLUTION-"].update(
                "Scan resolution (if applicable): " + str(values["-SCAN_RESOLUTION-"]) + "Hz")
            window_main["-SHOW_SCAN_WARMUP_TIME-"].update(
                "Pre-pump / warmup duration: " + str(values["-SCAN_WARMUP_TIME-"]) + "s")
            break
    window_scan_settings.close()


# Add/Remove Sensor Window
def tty_sensor_window():
    with open(os.path.join(file_path,'../data/sensor_params.json')) as json_file:
        sensor_params = json.load(json_file)
    with open(os.path.join(file_path,'../data/script_dict.json')) as json_file:
        script_dict = json.load(json_file)

    layout_sensor0 = sg.Frame("Serial Port: ttyUSB0", [
        [sg.Checkbox("enable", key="-SENSOR0ENABLED-", default=sensor_params["-SENSOR0ENABLED-"])],
        [sg.Text("Name: "), sg.In(sensor_params["-SENSOR0NAME-"], key="-SENSOR0NAME-")],
        [sg.Text("Serial No.: "), sg.In(sensor_params["-SENSOR0SN-"], key="-SENSOR0SN-")],
        [sg.Text("Baud Rate: "), sg.Combo(["4800", "9200", "19200", "no baud"], key="-SENSOR0BAUD-",
                                          default_value=sensor_params["-SENSOR0BAUD-"])],
        [sg.Text("Operating Script"), sg.Combo(list(script_dict.keys()), key="-SENSOR0SCRIPT-",
                                               default_value=sensor_params["-SENSOR0SCRIPT-"])]
    ])
    layout_sensor1 = sg.Frame("Serial Port: ttyUSB1", [
        [sg.Checkbox("enable", key="-SENSOR1ENABLED-", default=sensor_params["-SENSOR1ENABLED-"])],
        [sg.Text("Name: "), sg.In(sensor_params["-SENSOR1NAME-"], key="-SENSOR1NAME-")],
        [sg.Text("Serial No.: "), sg.In(sensor_params["-SENSOR1SN-"], key="-SENSOR1SN-")],
        [sg.Text("Baud Rate: "), sg.Combo(["4800", "9200", "19200", "no baud"], key="-SENSOR1BAUD-",
                                          default_value=sensor_params["-SENSOR1BAUD-"])],
        [sg.Text("Operating Script"), sg.Combo(list(script_dict.keys()), key="-SENSOR1SCRIPT-",
                                               default_value=sensor_params["-SENSOR1SCRIPT-"])]
    ])
    layout_sensor2 = sg.Frame("Serial Port: ttyUSB2", [
        [sg.Checkbox("enable", key="-SENSOR2ENABLED-", default=sensor_params["-SENSOR2ENABLED-"])],
        [sg.Text("Name: "), sg.In(sensor_params["-SENSOR2NAME-"], key="-SENSOR2NAME-")],
        [sg.Text("Serial No.: "), sg.In(sensor_params["-SENSOR2SN-"], key="-SENSOR2SN-")],
        [sg.Text("Baud Rate: "), sg.Combo(["4800", "9200", "19200", "no baud"], key="-SENSOR2BAUD-",
                                          default_value=sensor_params["-SENSOR2BAUD-"])],
        [sg.Text("Operating Script"), sg.Combo(list(script_dict.keys()), key="-SENSOR2SCRIPT-",
                                               default_value=sensor_params["-SENSOR2SCRIPT-"])]
    ])
    layout_sensor3 = sg.Frame("Serial Port: ttyUSB3", [
        [sg.Checkbox("enable", key="-SENSOR3ENABLED-", default=sensor_params["-SENSOR3ENABLED-"])],
        [sg.Text("Name: "), sg.In(sensor_params["-SENSOR3NAME-"], key="-SENSOR3NAME-")],
        [sg.Text("Serial No.: "), sg.In(sensor_params["-SENSOR3SN-"], key="-SENSOR3SN-")],
        [sg.Text("Baud Rate: "), sg.Combo(["4800", "9200", "19200", "no baud"], key="-SENSOR3BAUD-",
                                          default_value=sensor_params["-SENSOR3BAUD-"])],
        [sg.Text("Operating Script"), sg.Combo(list(script_dict.keys()), key="-SENSOR3SCRIPT-",
                                               default_value=sensor_params["-SENSOR3SCRIPT-"])]
    ])

    layout_add_sensor = [
        [[layout_sensor0, layout_sensor1], [layout_sensor2, layout_sensor3], sg.Button("Save"), sg.Button("Cancel")]
    ]

    window_ttyUSB_sensor = sg.Window("ttyUSB config", layout_add_sensor)
    while True:
        event, values = window_ttyUSB_sensor.read()
        if event == sg.WIN_CLOSED or event == 'Cancel':
            break
        elif event == "Save":
            json_file = json.dumps(values)
            # open file for writing, "w" 
            f = open("../data/sensor_params.json", "w")
            f.write(json_file)
            f.close()
            window_main["-ACTIVE_SENSOR_OVERVIEW-"].update(print_active_sensors())
            break

    window_ttyUSB_sensor.close()


# Add a new job
def add_job_window(cron):

    def current_job_view(cron):
        rows = []
        for i, job in enumerate(cron.crons):
            rows.append(
                [sg.Text("Job " + str(i) + ":\t" + job.command + "\t || " + job.comment), sg.Push(),
                 sg.Text(str(job.slices)),
                 sg.Checkbox("Enabled", key="-ENABLE_JOB_NR_" + str(i) + "-", default=job.enabled, enable_events=True),
                 sg.Button("Delete", key="-DELETE_JOB_NR_" + str(i) + "-")]
            )
        return rows

    def make_layout(cron):
        layout_add_job = [
            [sg.Text("Job cron string: "), sg.In("* * * * *", key="-NEW_JOB_CRON_STRING-")],
            [sg.Text("Job command: "), sg.Multiline("", size=(90, 4), key="-NEW_JOB_COMMAND-")],
            [sg.Text("Job comment: "), sg.Multiline("", size=(90, 4), key="-NEW_JOB_COMMENT-")],
            [sg.Button("Save"), sg.Button("Cancel")],
            [sg.Frame("current jobs", current_job_view(cron), key="-CURRENT_JOBS_FRAME-")]
        ]
        return layout_add_job
    window_new_job = sg.Window("Add new job", make_layout(cron))
    while True:
        event, values = window_new_job.read(timeout=3000)
        if event == sg.WIN_CLOSED or event == 'Cancel':
            break
        elif event == "Save":
            new_job = cron.new(command=values["-NEW_JOB_COMMAND-"], comment=values["-NEW_JOB_COMMENT-"])
            new_job.setall(values["-NEW_JOB_CRON_STRING-"])
            cron.write()
            window_new_job.close()
            window_new_job = sg.Window("Add new job", make_layout(cron))
        elif event[0:15] == "-DELETE_JOB_NR_" or event[0:15] == "-ENABLE_JOB_NR_":
            job_nr = int(event[-2])
            job_obj = cron[job_nr]
            if event[0:15] == "-DELETE_JOB_NR_" and sg.popup_yes_no('delete job '+event[-2:-1]+'?') == 'Yes':
                cron.remove(job_obj)
            elif event[0:15] == "-ENABLE_JOB_NR_":
                job_obj.enable(values[event])
            else:
                continue
            cron.write()
            window_new_job.close()
            window_new_job = sg.Window("Add new job", make_layout(cron))
    window_new_job.close()


# Reload/update 'terminal' logging data
def reload_logpaths():
    for i,val in enumerate([term0_modstatus,term1_modstatus,term2_modstatus,term3_modstatus]):
        if os.path.isfile(values["-LOG_"+str(i)+"_INPUT_PATH-"]):
            val = LogfilesCheckForUpdates(filepath=values["-LOG_"+str(i)+"_INPUT_PATH-"])
            with open(os.path.join(file_path,'../data/logfilepaths.txt'), 'r') as file:
                data = file.readlines()
            data[i] = values["-LOG_"+str(i)+"_INPUT_PATH-"] + "\n"
            with open(os.path.join(file_path,'../data/logfilepaths.txt'), 'w') as file:
                file.writelines(data)
            with open(val.filename) as file:
                window_main["-TERMINAL_OUTPUT_"+str(i)+"-"].update(file.read())
        else:
            window_main["-TERMINAL_OUTPUT_"+str(i)+"-"].update("not a valid logfile path")


# Step 3: Create Main Window
layout_main = main_window(get_scheduled_job_list(cron))
window_main = sg.Window("RasPi Lift Controller", layout_main)

# Step 4: Event loop
while True:
    event, values = window_main.read(timeout=300)

    if event == sg.WIN_CLOSED or event == 'Close':
        break
    elif event == "ttyUSB Devices":
        tty_sensor_window()
    elif event == "Change scan settings":
        change_scan_settings_window()
    elif event == "Run":
        sp = sg.execute_command_subprocess(values['-TERM_INPUT-'], pipe_output=True, wait=False)
        results = sg.execute_get_results(sp, timeout=40)
        window_main['-TERMINAL_OUTPUT_test-'].update(
            values["-TERMINAL_OUTPUT_test-"] + "\n >" + values['-TERM_INPUT-'] + "\n" + results[0])
        window_main['-TERM_INPUT-'].update("")
    elif event == "Run Job now":
        p = multiprocessing.Process(target=run_flntus_scan)
        p.start()
        p.join()
    elif event == "Add job":
        add_job_window(cron)
    elif event == "Reload Logpaths":
        reload_logpaths()

    check_for_logfile_changes()
    date = datetime.now()
    time_string = f'{date:%Y}-{date:%m}-{date:%d}  {date:%H}:{date:%M}:{date:%S}:{date:%f}'
    window_main["-JOB_FRAME_TITLE_CLOCK-"].update("Next jobs:\t" + time_string)
    
    new_cron_list=get_scheduled_job_list(cron)
    if  new_cron_list != values["-CRONJOBS-"]:
        window_main["-CRONJOBS-"].update(new_cron_list)

# close window
window_main.close()

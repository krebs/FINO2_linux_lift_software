"""
- documentation
- inter_byte_timeout?
"""
import serial
import time
import csv
import json
import os
import datetime


def run_flntus_scan(port_path='/dev/ttyUSB1',
                    folder_path=os.path.join(os.path.dirname(os.path.abspath(__file__)), r'../../output/')):
    """
        Does one profiling scan with parameters out of 'scan_params.json' and saves measured values into file
        :param port_path: System path to RS-232 port. eg. 'COM4' or '/dev/ttyUSB0'
        :param file_path: Path to log output file
        :return:
        """
    def add_to_logfiles(file_path_last_scan,path_complete_archive,path_daily_scans):
        with open(file_path_last_scan, "r", newline='') as f:
            last_scan_data = f.readlines()
        with open(path_complete_archive, "a", newline='') as f:
            f.writelines(last_scan_data)
            f.writelines("\n")
        with open(path_daily_scans, "a", newline='') as f:
            f.writelines(last_scan_data)
            f.writelines("\n")    
        
    path_complete_archive = os.path.join(folder_path, "FLNTUS_archive/FLNTUS_complete_archive.csv")
    file_path_last_scan = os.path.join(folder_path, "last_flntus_scan.csv")
    

    print("\n"+"running FLNTUS scan...")
    def stop_flntus(ser):
        for i in range(10):
            ser.write(b'!')
        # time.sleep(0.025) #if the 'stop' command does not work uncomment this line

    package_directory = os.path.dirname(os.path.abspath(__file__))
    with open(os.path.join(package_directory, r'../../data/sensor_params.json')) as f:
        sensor_params = json.load(f)
    with open(os.path.join(package_directory, r'../../data/scan_params.json')) as f:
        scan_params = json.load(f)
        warmup_t = float(scan_params['-SCAN_WARMUP_TIME-'])
        duration_t = float(scan_params['-SCAN_DURATION-'])

    try:
        ser = serial.Serial(port_path, 19200, timeout=1)  # open serial port windows
        time.sleep(1)
    except serial.serialutil.SerialException as e:
        print(e)
        with open(file_path_last_scan, "w", newline='') as f:
            writer = csv.writer(f, delimiter=",")
            writer.writerow([])
            writer.writerow(
                [str(e) + " " + time.strftime("%Y-%m-%dT%H:%M:%S%z", time.localtime())])
        return e

    # WARMUP---------------
    time.sleep(warmup_t) if warmup_t < 1 else time.sleep(2)
    # WARMUP---------------
    ser.reset_input_buffer()
    ser.flushOutput()
    ser.write(b'\r\n$run\r\n')

    ser.timeout = 5
    firstline = ser.read_until(expected=b'mvs 1\r\n')
    print(firstline)
    
    daily_filename = time.strftime("%Y-%m-%d_flntus_scans.csv", time.localtime())
    path_daily_scans = os.path.join(folder_path, "FLNTUS_archive/", daily_filename)
    if firstline == b'':
        with open(file_path_last_scan, "w", newline='') as f:
            writer = csv.writer(f, delimiter=",")
            writer.writerow([])
            writer.writerow(["Error: No signal from FLNTUS. (serial port okay?) at "+datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f%z")])
        add_to_logfiles(file_path_last_scan,path_complete_archive,path_daily_scans)
        raise Exception("Error: No signal from FLNTUS. (serial port okay?)")
    ser.timeout = 1
    t_start = time.time()
    
    #start_time = time.strftime("%Y-%m-%dT%H:%M:%S%z", time.localtime())

    with open(file_path_last_scan, "w", newline='') as f:
        writer = csv.writer(f, delimiter=",")
        writer.writerow([])
        writer.writerow(["FLNTUS profile: warmup start at " + time.strftime("%Y-%m-%dT%H:%M:%S%z", time.localtime())])
        writer.writerow('time(RasPi),time(FLNTUS),wavelength1[nm](Chl.),'
                        'count_Chl,wavelength2[nm](NTU),count_NTU,count_thermistor'.split(","))

    measurement_stopped = False
    while ser.inWaiting() != 0 or measurement_stopped == False:
        current_scan_time = -t_start + time.time()
        print("Scan progress: " + str(round(current_scan_time, 1)) + "/" + str(duration_t) + "s")
        data = ser.readline()
        K = data
        with open(file_path_last_scan, "a", newline='') as f:
            writer = csv.writer(f, delimiter=",")
            data_split = str(data.strip())[2:-1].split("\\t")
            data_line = [datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f%z")] + data_split
            writer.writerow(data_line)
        if current_scan_time >= duration_t and measurement_stopped == False:
            stop_flntus(ser)
            measurement_stopped = True

    data_left = False
    if ser.inWaiting() != 0:
        print("Noch ", ser.inWaiting(), " Bytes zu lesen")
        data_left = True
    # read out rest of buffer
    while data_left == True:
        data = ser.readline()
        with open(file_path_last_scan, "a", newline='') as f:
            writer = csv.writer(f, delimiter=",")
            writer.writerow(data)
        y = ser.inWaiting()
        print("Bytes in waiting: ", y)
        if y == 0:
            data_left = False

    time.sleep(0.5)
    data = ser.read_until(expected=b'Mem *\r\n')
    print(data)
    with open(file_path_last_scan, "a", newline='') as f:
        writer = csv.writer(f, delimiter=",")
        data_line = "End of profile: " + str(data)
        writer.writerow([data_line])

    ser.close()  # close port
    
    add_to_logfiles(file_path_last_scan,path_complete_archive,path_daily_scans)


#run_flntus_scan(port_path='/dev/ttyUSB1')

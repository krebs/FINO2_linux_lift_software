"""
-newline argument may not work on linux correctly, CHECK
-IN SITU PUMP DELAY NOT INCLUDED YET
-ERRORS FOR OTHER FILES
"""
import serial
import time
import csv
import json
import os
import sys
import datetime


def run_sbe19plus_scan(port_path='/dev/ttyUSB0',
                    folder_path=os.path.join(os.path.dirname(os.path.abspath(__file__)), r'../../output/')):

    """
    Does one profiling scan with parameters out of 'scan_params.json' and saves measured values into file
    :param port_path: System path to RS-232 port. eg. 'COM4' or '/dev/ttyUSB0'
    :param file_path: Path to log output file
    :return:
    """
    def add_to_logfiles(file_path_last_scan,path_complete_archive,path_daily_scans):
        with open(file_path_last_scan, "r", newline='') as f:
            last_scan_data = f.readlines()
        with open(path_complete_archive, "a", newline='') as f:
            f.writelines(last_scan_data)
            f.writelines("\n")
        with open(path_daily_scans, "a", newline='') as f:
            f.writelines(last_scan_data)
            f.writelines("\n")    
    
    path_complete_archive = os.path.join(folder_path, "SBE19_archive/sbe19_complete_archive.csv")
    file_path_last_scan = os.path.join(folder_path, "last_sbe_scan.csv")
    
    # load parameters for scan
    print("\n"+"running SBE19+ scan...")
    package_directory = os.path.dirname(os.path.abspath(__file__))
    with open(os.path.join(package_directory, r'../../data/sensor_params.json')) as f:
        sensor_params = json.load(f)
    with open(os.path.join(package_directory, r'../../data/scan_params.json')) as f:
        scan_params = json.load(f)
        warmup_t = float(scan_params['-SCAN_WARMUP_TIME-'])
        duration_t = float(scan_params['-SCAN_DURATION-'])
    try:
        ser = serial.Serial(port_path, 9400, timeout=1)  # open serial port windows
        time.sleep(1)
    except serial.serialutil.SerialException as e:
        print(e)
        # overwrite file and write header
        with open(file_path_last_scan, "w", newline='') as f:
            writer = csv.writer(f, delimiter=",")
            writer.writerow([])
            writer.writerow(
                [str(e) + " " + time.strftime("%Y-%m-%dT%H:%M:%S%z", time.localtime())])
        return e

    # WARMUP---------------
    time.sleep(warmup_t) if warmup_t < 1 else time.sleep(1)
    # WARMUP---------------

    ser.flushInput()
    print(ser.port)  # check which port was really used
    ser.write(b'poke!\r\n')  # wake up the SBE19 with a random command
    print(ser.read_until(expected=b"<Executed/>"))
    ser.write(b'stop\r\n')  # stop if already measuring
    firstline = ser.read_until(expected=b"<Executed/>")
    print(firstline)
    
    daily_filename = time.strftime("%Y-%m-%d_sbe19_scans.csv", time.localtime())
    path_daily_scans = os.path.join(folder_path, "SBE19_archive/", daily_filename)
    if firstline == b'':
        with open(file_path_last_scan, "w", newline='') as f:
            writer = csv.writer(f, delimiter=",")
            writer.writerow([])
            writer.writerow(["Error: No signal from SBE19+. (serial port okay?) at "+datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f%z")])
        add_to_logfiles(file_path_last_scan,path_complete_archive,path_daily_scans)
        raise Exception("Error: No signal from SBE19+. (serial port okay?)")
    
    with open(file_path_last_scan, "w", newline='') as f:
        writer = csv.writer(f, delimiter=",")
        writer.writerow(["SBE19p profile: warmup start at "+time.strftime("%Y-%m-%dT%H:%M:%S%z", time.localtime())])
        writer.writerow(["time", "temperature[C]", "conductivity[S/m]", "pressure[dbar]", "Oxygen[analog voltage]"])

    ser.timeout = 5
    ser.write(b'startnow\r\n')
    # sleep here for the pump delay value i guess? Looks like it only applies this real delay in situ
    print(ser.read_until(expected=b"<Executed/>"))
    t_start = time.time()
    ser.timeout = 1

    measurement_stopped = False
    while ser.inWaiting() != 0 or measurement_stopped == False:
        current_scan_time = -t_start + time.time()
        print("Scan progress: " + str(round(current_scan_time, 1)) + "/" + str(duration_t) + "s")
        data = ser.readline()
        # write into complete archive file
        with open(file_path_last_scan, "a", newline='') as f:
            writer = csv.writer(f, delimiter=",")
            data_split = [i.strip() for i in str(data.strip())[2:-1].split(",")]
            data_line = [datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f%z")]+data_split
            writer.writerow(data_line)
        if current_scan_time >= duration_t and measurement_stopped == False:
            ser.write(b'stop\r\n')
            measurement_stopped = True
    with open(file_path_last_scan, "a", newline='') as f:
        writer = csv.writer(f, delimiter=",")
        writer.writerow(["End of SBE19p profile: "+ str(ser.inWaiting()) + " bytes left in buffer"])

    print(ser.read_until(expected=b"<Executed/>"))
    ser.write(b'stop\r\n')
    print(ser.read_until(expected=b"<Executed/>"))
    ser.write(b'QS\r\n')
    ser.close()

    add_to_logfiles(file_path_last_scan,path_complete_archive,path_daily_scans)


    return True

# run_sbe19plus_scan(port_path='/dev/ttyUSB0')

